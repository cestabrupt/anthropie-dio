# ~/ABRÜPT/ANTHROPIE/DIO/*

La [page de ce livre](https://abrupt.ch/anthropie/dio/) sur le réseau.

## Sur le livre

Dio est une réécriture hackée des Bacchantes d’Euripide, un rêve insurrectionnel.

## Sur le collectif

[anthropie](http://anthropie.art) est un collectif d’écriture audiovisuelle pour enfants (sauvages)
 
[anthropie](http://anthropie.art) est un geste en constante augmentation
 
[anthropie](http://anthropie.art) est une cellule esthético-procrastino-insurrectionnelle

[anthropie](http://anthropie.art) prône le times new roman, l’anonymat, l’open-source, le DIY, le hack des machines et des esprits

## Sur la licence

Cet [antilivre](https://abrupt.ch/antilivre/) est dédié au domaine public. Il est mis à disposition selon les termes de la Licence Creative Commons Zero (CC0 1.0 Universel).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.ch) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.ch/partage/).
