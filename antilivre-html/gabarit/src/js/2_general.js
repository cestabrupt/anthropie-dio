// Scripts
const btn = document.querySelector('.shuffle');
const prose = document.querySelector('.prose');
const menu = document.querySelector('.menu');
// const orientation = document.querySelector('.orientation');
const info = document.querySelector('.button--info');

function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
}

function Poeme() {
  shuffleArray(dio);
  let texteFinal = "";

  for (let i = 0; i < dio[0].length; i++) {
    texteFinal += '<p class="prose__p">';
    texteFinal += dio[0][i];
    texteFinal += '</p>';
  }

  prose.innerHTML = texteFinal;

  let b = baffle('.prose p')
    .reveal(500)
    .set({
      characters: 'noussommestousteslefruitdespierresquisonnentlechaos',
      speed: 50
    });

  boxes();

}


btn.addEventListener('click', (e) => {
  e.preventDefault();
  Poeme();
});

menu.addEventListener('click', (e) => {
  e.preventDefault();
  let links = document.querySelectorAll('.title__link');
  links.forEach(function(el){
    el.classList.toggle('show');
  });
});

// orientation.addEventListener('click', (e) => {
//   e.preventDefault();
//   let sens = document.querySelector('.affiche');
//     sens.classList.toggle('affiche--horizontal');
// });

info.addEventListener('click', (e) => {
  e.preventDefault();
  let informations = document.querySelectorAll('.informations');
    informations.forEach(function(el){
      el.classList.toggle('informations--show');
    });
});


let downloadImage = document.querySelector('.button.download');
downloadImage.addEventListener('click', (e) => {
  e.preventDefault();
  document.documentElement.classList.add("hide-scrollbar");

  html2canvas(document.querySelector('.prose'),{
    allowTaint: false,
    logging: false,
    backgroundColor: "#000000",
    scale: 7
  }).then(function(canvas) {
    // const link = document.createElement('a');
    // document.body.appendChild(link);
    // link.download = 'dio.jpg';
    // link.href = canvas.toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream");
    // link.target = '_blank';
    // link.click();
    saveAs(canvas.toDataURL("image/jpeg"), 'dio.jpg');
  });

  document.documentElement.classList.remove("hide-scrollbar");
});


function saveAs(uri, filename) {
  const link = document.createElement('a');

  if (typeof link.download === 'string') {
    link.href = uri;
    link.download = filename;
    link.target = '_blank';
    document.body.appendChild(link);
    link.click();
    //remove the link when done
    document.body.removeChild(link);
  } else {
    window.open(uri);
  }
}


// target elements with the "draggable" class
interact('.affiche, .randombox, .prose p')
  .draggable({
    // enable inertial throwing
    inertia: true,
    // keep the element within the area of it's parent
    // modifiers: [
    //   interact.modifiers.restrictRect({
    //     restriction: 'parent',
    //     endOnly: true
    //   })
    // ],
    // enable autoScroll
    autoScroll: true,

    onstart: function (event) {
      event.target.style.zIndex = parseInt(new Date().getTime() / 1000);
    },
    // call this function on every dragmove event
    onmove: dragMoveListener,
    // call this function on every dragend event
    onend: function (event) {
      var textEl = event.target.querySelector('p')
    }
  })
  .resizable({
    // resize from all edges and corners
    edges: { left: true, right: true, bottom: true, top: true },

    modifiers: [
      // keep the edges inside the parent
      interact.modifiers.restrictEdges({
        outer: 'parent',
        endOnly: true
      }),

      // minimum size
      interact.modifiers.restrictSize({
        min: { width: 10, height: 10 }
      })
    ],

    inertia: true
  })
  .on('resizemove', function (event) {
    var target = event.target
    var x = (parseFloat(target.getAttribute('data-x')) || 0)
    var y = (parseFloat(target.getAttribute('data-y')) || 0)

    // update the element's style
    target.style.width = event.rect.width + 'px'
    target.style.height = event.rect.height + 'px'

    // translate when resizing from top or left edges
    x += event.deltaRect.left
    y += event.deltaRect.top

    target.style.webkitTransform = target.style.transform =
        'translate(' + x + 'px,' + y + 'px)'

    target.setAttribute('data-x', x)
    target.setAttribute('data-y', y)

  })

function dragMoveListener (event) {
  var target = event.target
  // keep the dragged position in the data-x/data-y attributes
  var x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx
  var y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy

  // translate the element
  target.style.webkitTransform =
    target.style.transform =
      'translate(' + x + 'px, ' + y + 'px)'

  // update the posiion attributes
  target.setAttribute('data-x', x)
  target.setAttribute('data-y', y)
}

// this is used later in the resizing and gesture demos
window.dragMoveListener = dragMoveListener

function randomNb(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function boxes() {
  const affiche = document.querySelector('.prose');
  const existingBoxes = document.querySelectorAll('.randombox');
  existingBoxes.forEach(function(el){
    el.remove();
  });
  for (var i = 0; i < randomNb(3,7); i++) {
    let randomBox = document.createElement("div");
    randomBox.style.zIndex = "-1";
    randomBox.className = "randombox";
    randomBox.style.width = 10 + randomNb(1,120) + "%";
    randomBox.style.height = 10 + randomNb(1,120) + "%";
    randomBox.style.top = 1 * randomNb(-20,100) + "%";
    randomBox.style.left = 1 * randomNb(-20,100) + "%";
    affiche.appendChild(randomBox);
	}
}

Poeme();
